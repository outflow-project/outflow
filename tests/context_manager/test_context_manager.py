# -*- coding: utf-8 -*-
from outflow.core.generic.context_manager import ContextManager


def test_context_manager():
    obj_a = object()
    obj_b = object()

    # create a first context with associated values
    with ContextManager(color="blue", number=42, obj=obj_a) as manager_a:
        assert manager_a.context == ContextManager.get_context()
        assert manager_a.parent_context == {}

        # spawn a nested context, overriding some of the values
        with ContextManager(color="yellow", obj=obj_b) as manager_b:
            assert manager_b.context == ContextManager.get_context()
            assert manager_b.parent_context == manager_a.context
            assert manager_b.context["obj"] is obj_b
            assert manager_b.context["number"] == 42

        # leave the nested context and check if the reset is successfull
        assert manager_b.context is None and manager_b.parent_context is None
        assert manager_a.context["obj"] is obj_a
