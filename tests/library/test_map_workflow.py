# -*- coding: utf-8 -*-
from typing import List

import pytest
from outflow.core.commands import Command
from outflow.core.pipeline import config
from outflow.core.target import Target
from outflow.core.tasks import Task, as_task
from outflow.core.test import CommandTestCase
from outflow.core.types import IterateOn
from outflow.library.workflows import MapWorkflow
from outflow.library.workflows.base_map_workflow import BaseMapWorkflow
from outflow.library.workflows.generic_map_workflow import determine_map_class


class MapException(Exception):
    pass


t1_message = "hello from t1"


@as_task
def T1() -> {"t1_out": str}:
    return t1_message


@as_task
def Generator1() -> {"identifiers": List[int]}:
    identifiers = [i for i in range(3)]
    return {"identifiers": identifiers}


@as_task
def Generator2(
    identifier: IterateOn("identifiers", type=int)
) -> {"identifiers2": List[int]}:
    identifiers = [i for i in range(identifier)]
    return {"identifiers2": identifiers}


@as_task
def Dummy(
    t1_out: str, identifier: IterateOn("identifiers2", type=int)
) -> {"external_edge_input": str}:
    return t1_out


@Target.output("out_array")
@as_task
def Generator():
    out_array = [i for i in range(3)]
    return {"out_array": out_array}


@Target.output("out_array1")
@Target.output("out_array2")
@as_task
def GeneratorMultiple():
    out_array1 = [i for i in range(3)]
    out_array2 = out_array1.copy()
    out_array2.reverse()
    return {"out_array1": out_array1, "out_array2": out_array2}


@as_task
def MultipleIteratedInputs(
    input_value1: IterateOn("out_array1", int),
    input_value2: IterateOn("out_array2", int),
) -> {"sum": int}:

    return {"sum": input_value1 + input_value2}


@as_task
def CheckMultipleIteratorOutputs(sum: List):
    for result in sum:
        if isinstance(result, Exception):
            raise result
    assert sum == [2, 2, 2]


@as_task
def CheckOutput(value: List):
    for result in value:
        if isinstance(result, Exception):
            raise result
    assert value == [0, 0, 0]


@as_task
def SimpleTaskAnnotation(
    input_value: IterateOn("out_array"),
) -> {"value": int}:
    return {"value": 0}


@as_task
def SimpleTaskAnnotationNoTypeError(
    input_value: IterateOn("out_array", int)
) -> {"value": int}:
    return {"value": 0}


@as_task
def SimpleTaskAnnotationTypeError(
    input_value: IterateOn("out_array", bool)
) -> {"value": int}:

    return {"value": 0}


@as_task
def SleepRaiseForValueEqual1(
    input_value: IterateOn("out_array", int)
) -> {"value": int}:
    if input_value != 1:
        raise MapException
    return {"value": 0}


def get_root_command(inner_task):
    class MapCommand(Command):
        def setup_tasks(self):
            generator = Generator()
            check = CheckOutput()

            with MapWorkflow(raise_exceptions=True, flatten_output=True) as m:
                inner_task()

            generator >> m >> check

            return check

    return MapCommand


class MapMultipleIteratedInputsCommandDefault(Command):
    def setup_tasks(self) -> List[Task]:
        generator_multiple = GeneratorMultiple()
        with MapWorkflow(raise_exceptions=True, flatten_output=False) as map_task:
            MultipleIteratedInputs()

        check_output = CheckMultipleIteratorOutputs()
        generator_multiple >> map_task >> check_output
        return check_output


@as_task
def RaiseKeyError(input_value: IterateOn("out_array", int)) -> {"value": int}:
    raise KeyError(f"key error with value '{input_value}'")


class TestDefaultMap(CommandTestCase):
    custom_config = {}

    def test_map_force_raise_exception(self):
        config.update(self.custom_config)
        self.root_command_class = get_root_command(RaiseKeyError)
        with pytest.raises(KeyError):
            self.run_command([])

    def test_map_with_decorator(self):
        self.root_command_class = get_root_command(SimpleTaskAnnotation)

        config.update(self.custom_config)

        self.run_command([])

    def test_map_with_annotation(self):
        self.root_command_class = get_root_command(SimpleTaskAnnotation)

        config.update(self.custom_config)

        self.run_command([])

    def test_map_with_annotation_type_error(self):
        self.root_command_class = get_root_command(SimpleTaskAnnotationTypeError)

        config.update(self.custom_config)

        with pytest.raises(TypeError):
            self.run_command([])

    def test_map_with_exception(self):
        self.root_command_class = get_root_command(SleepRaiseForValueEqual1)

        config.update(self.custom_config)

        with pytest.raises(MapException):
            self.run_command([])

    def test_map_with_annotation_no_type_error(self):
        self.root_command_class = get_root_command(SimpleTaskAnnotationNoTypeError)

        config.update(self.custom_config)

        self.run_command([])

    def test_map_multiple_iterateon(self):
        class MapMultipleIteratedInputsCommand(Command):
            def setup_tasks(self) -> List[Task]:
                generator_multiple = GeneratorMultiple()
                with MapWorkflow(raise_exceptions=True) as map_task:
                    MultipleIteratedInputs()

                check_output = CheckMultipleIteratorOutputs()
                generator_multiple >> map_task >> check_output
                return check_output

        self.root_command_class = MapMultipleIteratedInputsCommand
        config.update(self.custom_config)

        self.run_command([])

    def test_map_multiple_start_and_end_tasks(self):
        @as_task
        def CheckOutput():
            pass

        class MyCommand(Command):
            def setup_tasks(self):
                with MapWorkflow(raise_exceptions=True) as map_task:
                    SimpleTaskAnnotation()
                    T1()

                Generator() >> map_task

                return map_task

        self.root_command_class = MyCommand
        config.update(self.custom_config)

        code, result = self.run_command([])

        assert result == [
            {
                "value": [0, 0, 0],
                "t1_out": ["hello from t1", "hello from t1", "hello from t1"],
            }
        ]

    def test_map_external_edge(self):
        @as_task
        def ReturnExternalEdgeInput(
            out: IterateOn("out_array"), t1_out: str
        ) -> {"external_input_val": str}:
            return {"external_input_val": t1_out}

        class MapExternalEdgeCommand(Command):
            def setup_tasks(self) -> List[Task]:
                generator = Generator()
                t1 = T1()
                with MapWorkflow(raise_exceptions=True) as map_task:
                    ret_ext = ReturnExternalEdgeInput()
                    t1 >> ret_ext

                generator >> map_task
                return map_task

        self.root_command_class = MapExternalEdgeCommand
        config.update(self.custom_config)

        _, result = self.run_command([])

        assert result == [{"external_input_val": [t1_message, t1_message, t1_message]}]

    def test_map_nested_external_edge(self):
        class MapNestedExternalEdgeCommand(Command):
            def setup_tasks(self):
                generator1 = Generator1()
                t1 = T1()

                with MapWorkflow(name="map1", raise_exceptions=True) as map1:
                    # print_words = PrintWords()
                    # t1 >> print_words
                    generator2 = Generator2()

                    with MapWorkflow(name="map2", raise_exceptions=True) as map2:
                        dummy = Dummy()
                        t1 >> dummy

                    generator2 >> map2
                #
                generator1 >> map1

                return map1

        self.root_command_class = MapNestedExternalEdgeCommand
        config.update(self.custom_config)

        _, result = self.run_command([])

        expected = [
            {
                "external_edge_input": [
                    [],
                    ["hello from t1"],
                    ["hello from t1", "hello from t1"],
                ]
            }
        ]
        assert result == expected

    def test_map_no_outputs_argument(self):
        class MapNoOutputsCommand(Command):
            def setup_tasks(self):

                generator = Generator()

                with MapWorkflow(no_outputs=True, raise_exceptions=True) as m:
                    SimpleTaskAnnotation()

                generator >> m

                return m

        self.root_command_class = MapNoOutputsCommand

        config.update(self.custom_config)

        _, result = self.run_command([])

        assert result == [None]

    def test_map_inherit_class(self):
        from outflow.library.workflows.sequential_map_workflow import (
            SequentialMapWorkflow,
        )

        class MyMap(SequentialMapWorkflow):
            pass

        class MapNoOutputsCommand(Command):
            def setup_tasks(self):

                generator = Generator()

                with MyMap(no_outputs=True) as m:
                    SimpleTaskAnnotation()

                generator >> m

                return m

        self.root_command_class = MapNoOutputsCommand

        config.update(self.custom_config)

        _, result = self.run_command([])

        assert result == [None]

    def test_map_initial_inputs(self):
        from outflow.library.workflows import MapWorkflow

        @as_task
        def SomeTask(value, extra_input):
            assert value in [0, 1, 2]
            assert extra_input == "extra_input"

        @Target.output("out_array")
        @Target.output("extra_input")
        @as_task
        def GeneratorWithExtraInput():
            out_array = [i for i in range(3)]
            return {"out_array": out_array, "extra_input": "extra_input"}

        @as_task
        def MappedTaskWithUnmappedInput(
            input_value: IterateOn("out_array"), extra_input: str
        ):
            assert input_value in [0, 1, 2]
            assert extra_input == "extra_input"

        class MapNoOutputsCommand(Command):
            def setup_tasks(self):

                generator = GeneratorWithExtraInput()

                with MapWorkflow(no_outputs=True, raise_exceptions=True) as m:
                    MappedTaskWithUnmappedInput()

                generator >> m

                return m

        self.root_command_class = MapNoOutputsCommand

        config.update(self.custom_config)

        _, result = self.run_command([])

        assert result == [None]

    def test_map_inherit_class_outputs(self):
        class MapNoOutputsCommand(Command):
            def setup_tasks(self):
                class MyMap(determine_map_class()):
                    def __init__(self: BaseMapWorkflow, *args, **kwargs):
                        super().__init__(*args, **kwargs)
                        self.add_output("my_map_output", type=List)
                        self.add_output("another_output", type=str)

                    def reduce(self, output):
                        return {
                            "my_map_output": output,
                            "another_output": "some_string",
                        }

                generator = Generator()

                with MyMap() as m:
                    SimpleTaskAnnotation()

                generator >> m

                return m

        self.root_command_class = MapNoOutputsCommand

        config.update(self.custom_config)

        _, result = self.run_command([])

        assert result == [
            {
                "my_map_output": [[{"value": 0}], [{"value": 0}], [{"value": 0}]],
                "another_output": "some_string",
            }
        ]

    def test_map_flatten(self):
        # test to find if a Flatten task in library would be useful
        class MapCommand(Command):
            def setup_tasks(self):
                generator = Generator()

                with MapWorkflow(raise_exceptions=True, flatten_output=True) as m:
                    SimpleTaskAnnotation()

                generator >> m

                return m

        self.root_command_class = MapCommand

        config.update(self.custom_config)

        code, result = self.run_command([])

        assert result == [{"value": [0, 0, 0]}]

    def test_map_wrong_input_name(self):
        @as_task
        def SimpleTaskAnnotation(out: IterateOn("wrong_name")) -> {"value": int}:
            return {"value": 0}

        class MapCommand(Command):
            def setup_tasks(self):
                generator = Generator()

                with MapWorkflow(raise_exceptions=True, flatten_output=True) as m:
                    SimpleTaskAnnotation()

                generator >> m

                return m

        self.root_command_class = MapCommand

        config.update(self.custom_config)

        from outflow.core.exceptions import IOCheckerError

        with pytest.raises(IOCheckerError):
            code, result = self.run_command([])

    def test_map_auto_map_on(self):
        @as_task
        def SimpleTask(out: int) -> {"value": int}:
            return {"value": 0}

        class MapCommand(Command):
            def setup_tasks(self):
                generator = Generator()

                with MapWorkflow(raise_exceptions=True, flatten_output=True) as m:
                    SimpleTask()

                generator >> m

                return m

        self.root_command_class = MapCommand

        config.update(self.custom_config)

        code, result = self.run_command([])
        assert result == [{"value": [0, 0, 0]}]

    def test_map_auto_map_on_fail_multiple_inputs(self):
        @as_task
        def SimpleTaskMultipleInputs(out: int, other: int) -> {"value": int}:
            return {"value": 0}

        class MapCommand(Command):
            def setup_tasks(self):
                generator = Generator()

                with MapWorkflow(raise_exceptions=True, flatten_output=True) as m:
                    SimpleTaskMultipleInputs()

                generator >> m

                return m

        self.root_command_class = MapCommand

        config.update(self.custom_config)

        with pytest.raises(AttributeError):
            code, result = self.run_command([])

    def test_map_explicit_map_on(self):
        @as_task
        def SimpleTaskMultipleInputs(out: int, other: int) -> {"value": int}:
            return {"value": 0}

        class MapCommand(Command):
            def setup_tasks(self):
                generator = Generator()

                with MapWorkflow(
                    raise_exceptions=True,
                    flatten_output=True,
                    map_on={"out_array", "out"},
                ) as m:
                    SimpleTaskMultipleInputs()

                generator >> m

                return m

        self.root_command_class = MapCommand

        config.update(self.custom_config)

        with pytest.raises(AttributeError):
            code, result = self.run_command([])


class TestParallelBackend(TestDefaultMap):
    custom_config = {
        "logging": {"version": 1},
        "backend": "parallel",
    }

    def test_map_nested_external_edge(self):
        # expecting : NotImplementedError: Nested MapWorkflows are not supported with parallel backend
        with pytest.raises(NotImplementedError):
            super().test_map_nested_external_edge()


@pytest.mark.slurm
class TestSlurmBackend(TestDefaultMap):
    force_dry_run = False
    custom_config = {
        "logging": {"version": 1},
        "backend": "slurm",
        "databases": {
            "default": {
                "dialect": "postgresql",
                "admin": "pipeadmin:adminpwd",
                "user": "pipeuser:userpwd",
                "address": "postgres",
                "database": "outflow_test",
            }
        },
    }
