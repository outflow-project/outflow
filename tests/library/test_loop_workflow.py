# -*- coding: utf-8 -*-
from outflow.core import exit_pipeline
from outflow.core.commands import Command
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase
from outflow.library.workflows import LoopWorkflow


@as_task
def Example(nb_called=[]):
    nb_called.append(0)
    if len(nb_called) > 10:
        exit_pipeline()


class TestLoopTask(CommandTestCase):
    def test_loop_iterations(self):
        class LoopCommand(Command):
            def setup_tasks(self):
                with LoopWorkflow(iterations=2):
                    Example()

        self.root_command_class = LoopCommand

        _, result = self.run_command([])

    def test_loop_infinite_until_pipeline_stop(self):
        class LoopCommand(Command):
            def setup_tasks(self):
                with LoopWorkflow(infinite=True):
                    Example()

        self.root_command_class = LoopCommand

        _, result = self.run_command([])
