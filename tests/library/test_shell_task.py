# -*- coding: utf-8 -*-
import platform
import shlex
from unittest import mock

import pytest
from outflow.core.commands import Command, argument
from outflow.core.pipeline import context
from outflow.library.tasks.shell_tasks import (
    PopenTask,
    ExecuteShellScripts,
    IPythonTask,
)
from outflow.core.target import Target
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase


@Target.output("command")
@as_task
def GenerateAlembicCommand():
    argv = shlex.split(context.args.argv.strip("\"'"))

    return {"command": ["alembic", *argv]}


@argument(
    "-a",
    "--argv",
    type=str,
    help="A string containing alembic CLI args (be sure to double quote the string to avoid issues, e.g. \"'-h --raiseerr'\"",
)
class Alembic(Command):
    """
    Alembic wrapper
    """

    def setup_tasks(self):

        generate_alembic_command = GenerateAlembicCommand()
        exec_task = PopenTask()

        generate_alembic_command >> exec_task

        return exec_task


class SomeUnixCommand(Command):
    """
    Test somme wrapped unix commands
    """

    def setup_tasks(self):
        return ExecuteShellScripts(shell_scripts=["ls -ll", "ls"])


class IPythonShellCommand(Command):
    """
    Test that the IPython shell is invoked correctly
    """

    def setup_tasks(self):
        return IPythonTask()


class TestCommandWithShellTask(CommandTestCase):
    def test_alembic_command(self):

        self.root_command_class = Alembic
        self.run_command(["--argv", '"-h --raiseerr"'])

    @pytest.mark.skipif(
        platform.system() == "Windows",
        reason="Unix commands are not available on Windows",
    )
    def test_unix_commands(self):

        self.root_command_class = SomeUnixCommand
        self.run_command([])

    def test_ipython_shell(self, capfd):
        def start_ipython_strip_kwargs(*args, **kwargs):
            from IPython.testing.globalipapp import start_ipython

            return start_ipython()

        with mock.patch("IPython.start_ipython", start_ipython_strip_kwargs):
            self.root_command_class = IPythonShellCommand
            self.run_command([])
