# -*- coding: utf-8 -*-
import signal
import time


class GracefulKiller:
    kill_now = False

    def __init__(self):
        print("init killer", flush=True)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        print("INSIDE SIGNAL HANDLER", flush=True)
        self.kill_now = True


if __name__ == "__main__":

    killer = GracefulKiller()

    while True:
        time.sleep(1)
        if killer.kill_now:
            break

    with open("hello.txt", "w") as f:
        f.write("successfully exited")
