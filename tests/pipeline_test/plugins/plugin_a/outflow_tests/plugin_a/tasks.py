# -*- coding: utf-8 -*-
import json
from typing import List

from outflow.core.logging import logger
from outflow.core.pipeline import context, settings
from outflow.core.tasks import as_task
from outflow.core.types import IterateOn
from outflow_tests.plugin_a.models.dictionary import Dictionary


@as_task(with_self=True)
def T1(self) -> {"out": str}:
    return "test_val_out"


@as_task
def WriteResult(identifier: List, out: List) -> None:
    output_dir = settings.TEMP_DIR / "map_output.json"
    logger.info(f"writting map_output={(identifier, out)} in {output_dir}")
    with open(output_dir, "w") as outfile:
        json.dump((identifier, out), outfile)


@as_task
def DeleteAll():
    context.session.query(Dictionary).delete()


@as_task(with_self=True)
def Example(
    self, identifier: IterateOn("identifiers", type=int), out: str = "test_default_val"
):
    logger.info(f"hello from example task with {identifier=}")
    logger.debug("test debug")
    # exit_pipeline()
    return {"identifier": identifier, "out": out}


@as_task(with_self=True)
def Insert(self, identifier: IterateOn("identifiers", type=int)):
    Dictionary.create(identifier=identifier, word=f"hello{identifier}")

    return {"identifier": identifier}


@as_task
def Select(identifier: int):
    logger.info(Dictionary.one(identifier=identifier))


@as_task(with_self=True)
def Generator1(self) -> {"identifiers": List[int]}:
    identifiers = [i for i in range(3)]
    return {"identifiers": identifiers}


@as_task(with_self=True)
def Generator2(
    self, identifier: IterateOn("identifiers", type=int)
) -> {"identifiers": List[int]}:
    identifiers = [i for i in range(3)]
    return {"identifiers": identifiers}


@as_task(with_self=True)
def PrintWords(self, out: str, identifier: IterateOn("identifiers", type=int)):

    logger.info(f"Identifier : {identifier}")
    logger.info(f"{self.workflow}")
    logger.info(f"{out=}")

    return {}
