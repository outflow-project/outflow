# -*- coding: utf-8 -*-
import random
from typing import List, Union

from outflow.core.commands import Command, RootCommand
from outflow.core.logging import logger
from outflow.core.tasks import as_task
from outflow.library.workflows import LoopWorkflow, MapWorkflow
from outflow_tests.plugin_a.tasks import (
    T1,
    DeleteAll,
    Example,
    Generator1,
    Generator2,
    Insert,
    Select,
    WriteResult,
)

from outflow.library.workflows import IterativeWorkflow
from outflow.library.workflows.sequential_map_workflow import SequentialMapWorkflow


@RootCommand.subcommand(backend="slurm")
class TestMap(Command):
    def setup_tasks(self):

        generator1 = Generator1()
        t1 = T1()

        with MapWorkflow(name="map1", raise_exceptions=True) as map1:
            # print_words = PrintWords()
            # t1 >> print_words
            generator2 = Generator2()

            with MapWorkflow(name="map2", raise_exceptions=True) as map2:
                dummy = Example()

                t1 >> dummy

            generator2 >> map2

        write_result = WriteResult()
        generator1 >> map1 >> write_result
        return write_result


@RootCommand.subcommand(backend="default")
class TestSlurm(Command):
    def setup_tasks(self):

        generator1 = Generator1()
        t1 = T1()

        write_result = WriteResult()

        with MapWorkflow(name="map1") as map1:
            dummy = Example()

            t1 >> dummy

        generator1 >> map1 >> write_result

        return [
            write_result,
        ]


@RootCommand.subcommand(backend="parallel")
class TestParallelSqlite(Command):
    def setup_tasks(self):

        generator1 = Generator1()

        with MapWorkflow(name="map1") as map1:
            Insert() >> Select()

        DeleteAll() >> generator1 >> map1


@as_task
def A(num=0):
    logger.info("inside A")


@as_task
def B():
    logger.info("inside BBBBBBB")


@RootCommand.subcommand()
class TestWorkflow(Command):
    def setup_tasks(self):
        a = A()
        b = B()

        a >> b


@RootCommand.subcommand()
class TestOrder(Command):
    def setup_tasks(self):

        B()
        A()


@as_task
def Random() -> {"val": int}:
    rand_val = random.randint(0, 10)
    logger.info(f"random : {rand_val}")
    return rand_val


@as_task
def PrintValues(map_outputs: str):
    logger.info(f"{map_outputs=}")


@RootCommand.subcommand()
class TestRepeat(Command):
    def setup_tasks(self):
        with LoopWorkflow(iterations=3):
            Random()


# @RootCommand.subcommand()
# class InfiniteWorkflow(Command):
#


@RootCommand.subcommand(backend="default")
class TestLoop(Command):
    def setup_tasks(self):

        with LoopWorkflow(iterations=10) as loop:
            Example(identifier=0)

        loop >> Example(identifier=1)


def break_func(iter_value):
    return iter_value > 500


@as_task
def RetA() -> {"a_output": str}:
    return "a"


@as_task
def GenList(
    iter_value: Union[int, None]
) -> {"iter_value": Union[int, None], "l": List[int]}:
    return {
        "l": [1, 2, 3],
        "iter_value": iter_value,
    }


@as_task
def IterTaskMapWorkflowInside(
    a_output: str, iter_value: Union[int, None], some_int: int
) -> {"inside_output": str, "iter_value": int}:
    assert a_output == "a"
    assert some_int in [1, 2, 3]

    if iter_value is None:
        crit = 0
    else:
        crit = iter_value + 1

    return {"inside_output": "my_output", "iter_value": crit}


@as_task
def Reduce(map_output: List) -> {"iter_value": int}:
    return sum([map_out[0]["iter_value"] for map_out in map_output])


@RootCommand.subcommand(backend="default")
class IterativeCommand(Command):
    def setup_tasks(self):
        a = RetA()
        with IterativeWorkflow(max_iterations=10, break_func=break_func) as iter_task:
            with MapWorkflow(raise_exceptions=True) as map_task:
                a >> IterTaskMapWorkflowInside()

            GenList() >> map_task >> Reduce()

        return iter_task


@as_task
def PrintData(computation_result: list):
    logger.info(f"Result of the computation: {computation_result}")


@as_task
def GenData() -> {"some_data": list}:
    some_data = list(range(15))
    return {"some_data": some_data}


@as_task
def Compute(some_data):
    logger.info(f"Running computation for {some_data=}")
    # Simulate a big computation
    result = some_data * 2

    # return the result for the next task
    return {"computation_result": result}


# create a workflow
@MapWorkflow.as_workflow(cache=True, output_name="computation_result")
def compute_workflow():
    Compute()


@RootCommand.subcommand(backend="parallel")
class ComputeData(Command):
    def setup_tasks(self):
        # setup the main workflow by using the workflow defined outside command

        # create a workflow

        compute_workflow = SequentialMapWorkflow(
            cache=True, output_name="computation_result"
        )

        compute_workflow.start()
        Compute()
        compute_workflow.stop()

        GenData() | compute_workflow | PrintData()
