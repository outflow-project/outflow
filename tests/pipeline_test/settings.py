# -*- coding: utf-8 -*-
import pathlib

PLUGINS = ["outflow.management", "outflow_tests.plugin_a"]

TEMP_DIR = pathlib.Path(__file__).parent.resolve() / ".outflow_tmp"
