# -*- coding: utf-8 -*-
import copy
import os
import pathlib
import shutil
import sys
import tempfile
from typing import List
from uuid import uuid4

import pytest
import sqlalchemy
from outflow.core.commands import RootCommand
from outflow.core.pipeline import config, context, settings
from outflow.core.test.test_cases import DatabaseCommandTestCase
from outflow.core.types import IterateOn
from outflow.library.workflows import MapWorkflow
from outflow.core.tasks import as_task
from outflow.core.commands import Command


class TestDatabase(DatabaseCommandTestCase):
    def run_command(self, arg_list, **kwargs):
        return super().run_command(arg_list, force_dry_run=False, **kwargs)

    def setup_method(self):
        """
        Setup the pipeline before each test
        :return:
        """
        super().setup_method()

        # backup the python path
        self._initial_python_path = copy.copy(sys.path)
        self._initial_modules = set(sys.modules)

        # create a temp dir to store the database
        self.temp_dir_path = tempfile.mkdtemp(prefix="test_outflow_")

    def teardown_method(self):
        super().teardown_method()

        # reset the python path and remove the test plugin

        for key in set(sys.modules) - self._initial_modules:
            if key.startswith("my_namespace.my_plugin") or key == "my_namespace":
                del sys.modules[key]

        sys.path = self._initial_python_path

        # since the database is created in a temp directory, we can use
        # the ignore errors flag to avoid raising errors, especially
        # the PermissionError on Windows (due to SQLAlchemy locking the database file)
        shutil.rmtree(self.temp_dir_path, ignore_errors=True)
        self.temp_dir_path = None

    def test_management_debug(self, capfd):
        class SomeException(Exception):
            pass

        @as_task
        def Raise():
            raise SomeException("Some error")

        @RootCommand.subcommand()
        class RunTasksWithException(Command):
            def setup_tasks(self):
                Raise()

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # upgrade
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        self.run_command(upgrade_arg_list)

        with pytest.raises(SomeException):
            self.run_command(["run_tasks_with_exception"])

        self.run_command(["management", "debug", "1"])

        out, err = capfd.readouterr()

        # clean error messages to avoid issues when comparing with the expected output
        cleaned_output = " ".join([line.strip() for line in out.split("\n")])

        messages = ["SomeException: Some error", "Reason :", "Task raise", "failed"]

        for message in messages:
            assert message in cleaned_output

    def test_sqlite_migration_upgrade(self):
        @RootCommand.subcommand()
        def CheckUpgrade():
            from outflow.core.pipeline import context
            from outflow.management.models.configuration import Configuration
            from outflow.management.models.block import Block

            engine = context._databases["default"].engine
            tables = sqlalchemy.inspect(engine).get_table_names()
            assert "outflow_block" in tables
            assert "outflow_runtime_exception" in tables

            session = context.session
            assert len(session.query(Block).all()) == 3
            assert len(session.query(Configuration).all()) == 1
            # 2 because migrations have inserted a dummy configuration

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # apply the migration
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        # check if the outflow tables have been created
        check_arg_list = ["check_upgrade"]
        error_code, result = self.run_command(check_arg_list)

    def test_db_handlers(self):

        from outflow.core.db.handlers import get_or_create, one
        from outflow.management.models.block import Block

        @RootCommand.subcommand()
        def CheckGetOrCreate():

            session = context.session
            Block.create(
                plugin="test_plugin",
                name="test_block",
                run=context.db_run,
                type="task",
                uuid=str(uuid4()),
                input_targets={},
                output_targets={},
                parent_id=None,
            )
            task_obj = get_or_create(session, Block, plugin="test_plugin")
            assert task_obj.name == "test_block"

        @RootCommand.subcommand()
        def CheckOne():
            session = context.session
            task_obj = one(session, Block, plugin="test_plugin")
            assert task_obj.name == "test_block"

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # upgrade
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        check_get_or_create = ["check_get_or_create"]
        error_code, result = self.run_command(check_get_or_create)

        check_one = ["check_one"]
        error_code, result = self.run_command(check_one)

    def test_db_content(self):

        from outflow.management.models.block import Block
        from outflow.core.tasks import as_task

        @as_task
        def Task1() -> {"message": str}:
            return "Hello world!"

        @as_task
        def Task2(message: str, smth_with_default_value: str = "value"):
            assert message == "Hello world!"

        from outflow.core.commands import Command
        from outflow.core.workflow import Workflow

        @RootCommand.subcommand()
        class RunTasks(Command):
            def setup_tasks(self):

                with Workflow(name="parent_workflow"):
                    with Workflow(name="child_workflow"):
                        Task1() >> Task2()

        @RootCommand.subcommand()
        def CheckContent():
            session = context.session

            # check tasks
            tasks_in_db = session.query(Block).filter(Block.type == "task").all()
            assert len(tasks_in_db) == 4
            task1_db: Block = session.query(Block).filter(Block.name == "task1").one()
            task2_db: Block = session.query(Block).filter(Block.name == "task2").one()

            assert len(task1_db.downstream_blocks) == 1
            assert task2_db in task1_db.downstream_blocks

            assert len(task2_db.upstream_blocks) == 1
            assert task1_db in task2_db.upstream_blocks

            # check workflows
            workflows_in_db = (
                session.query(Block).filter(Block.type == "workflow").all()
            )

            # 1st command:
            # top_level_workflow (workflow)
            #  └─ parent_workflow (workflow)
            #    └─ child_workflow (workflow)
            #      └─ task1 >> task2 (tasks)

            # 2nd command:
            # top_level_workflow (workflow)
            #  └─ check_content (task)

            # so 4 workflows total
            assert len(workflows_in_db) == 4

            parent_workflow = (
                session.query(Block).filter(Block.name == "parent_workflow").one()
            )
            child_workflow = (
                session.query(Block).filter(Block.name == "child_workflow").one()
            )

            # check workflows nesting
            assert task1_db.parent == child_workflow
            assert child_workflow.parent == parent_workflow
            assert child_workflow in parent_workflow.children
            assert parent_workflow.parent == workflows_in_db[0]

            # check tasks input and outputs
            assert task1_db.input_targets == {}
            assert task1_db.output_targets == {"message": {"type": "str"}}

            assert task2_db.input_targets == {
                "message": {
                    "type": "str",
                    "default": "<class 'outflow.core.target.NoDefault'>",
                },
                "smth_with_default_value": {"type": "str", "default": "'value'"},
            }
            assert task2_db.output_targets == {}

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # upgrade
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        run_tasks = ["run_tasks"]
        error_code, result = self.run_command(run_tasks)

        check_content = ["check_content"]
        error_code, result = self.run_command(check_content)

    def test_db_content_exception(self):
        class SomeException(Exception):
            pass

        @as_task
        def Raise():
            raise SomeException("Some error")

        from outflow.core.commands import Command

        @RootCommand.subcommand()
        class RunTasksWithException(Command):
            def setup_tasks(self):
                Raise()

        @RootCommand.subcommand()
        def CheckExceptionContent():
            session = context.session

            from outflow.management.models.runtime_exception import RuntimeException

            runtime_exceptions_in_db = session.query(RuntimeException).all()

            assert len(runtime_exceptions_in_db) == 1

            db_exception: RuntimeException = runtime_exceptions_in_db[0]

            assert db_exception.block_id == 2
            assert db_exception.exception_type == "SomeException"
            assert (
                db_exception.exception_msg
                == "tests.core.test_database.TestDatabase.test_db_content_exception.<locals>.SomeException: Some error"
            )

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        with pytest.raises(SomeException):
            self.run_command(["run_tasks_with_exception"])

        self.run_command(["check_exception_content"])

    def test_db_content_map(self):

        from outflow.management.models.block import Block

        from outflow.core.tasks import as_task

        @as_task
        def Task1(num: IterateOn("nums", type=int)) -> {"num": int}:
            return num

        @as_task
        def Gen1() -> {"nums": List[int]}:
            return [1, 2, 3]

        @as_task
        def Gen2(num: IterateOn("nums", type=int)) -> {"nums": List[int]}:
            return [4, 5, 6]

        from outflow.core.commands import Command

        @RootCommand.subcommand()
        class RunTasks(Command):
            def setup_tasks(self):

                with MapWorkflow(name="parent_map") as parent_map:
                    with MapWorkflow(name="child_map") as child_map:
                        Task1()
                    Gen2() >> child_map
                Gen1() >> parent_map

                return parent_map

        @RootCommand.subcommand()
        def CheckContent():
            session = context.session

            blocks = session.query(Block).all()

            assert blocks[0].name == "top_level_workflow"

            gen1 = session.query(Block).filter_by(name="gen1").one()
            assert gen1.parent == blocks[0]

            parent_map = session.query(Block).filter_by(name="parent_map").one()
            assert parent_map.input_values == [{"num": "1"}, {"num": "2"}, {"num": "3"}]
            assert parent_map.parent == blocks[0]
            assert len(parent_map.children) == 6  # 3 gen2 + 3 child_map

            child_maps = session.query(Block).filter_by(name="child_map").all()
            assert len(child_maps) == 3
            for child_map in child_maps:
                assert child_map.parent == parent_map
                assert child_map.input_values == [
                    {"num": "4"},
                    {"num": "5"},
                    {"num": "6"},
                ]
                assert len(child_map.children) == 3

            task1s = session.query(Block).filter_by(name="task1").all()
            assert len(task1s) == 9
            for task1 in task1s:
                assert task1 in task1.parent.children

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # upgrade
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        run_tasks = ["run_tasks"]
        error_code, result = self.run_command(run_tasks)

        assert result == [{"num": [[4, 5, 6], [4, 5, 6], [4, 5, 6]]}]

        check_content = ["check_content"]
        error_code, result = self.run_command(check_content)

    def test_db_handlers_on_another_db(self):

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test_default.db"),
            },
            "another": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test_another.db"),
            },
        }

        from outflow.core.db import Model
        from sqlalchemy import Column, Integer, String

        class Foo(Model):
            __bind_key__ = "default"
            name = Column(String)
            id = Column(Integer, primary_key=True)

        class Bar(Model):
            __bind_key__ = "another"
            name = Column(String)
            id = Column(Integer, primary_key=True)

        class Baz(Model):
            name = Column(String)
            id = Column(Integer, primary_key=True)

        @RootCommand.subcommand()
        def CheckDefaultDb():
            Foo.create(name="foo")
            foo = Foo.one(name="foo")
            Baz.create(name="baz")
            baz = Baz.one(name="baz")
            assert foo.name == "foo"
            assert baz.name == "baz"

        @RootCommand.subcommand()
        def CheckAnotherDb():
            Bar.create(name="bar")
            bar = Bar.one(name="bar")
            assert bar.name == "bar"

        # apply the migrations
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        # create the tables for the custom models (not handled by the migration system)
        custom_models = [Foo, Bar, Baz]

        with self.pipeline_db_session():
            for database in context.databases.values():
                Model.metadata.create_all(
                    bind=database.engine, tables=database.get_tables()
                )

        # test the commands
        check_default_db = ["check_default_db"]
        # ensure the context contains the custom models to handle register/unregister properly
        context._models = custom_models
        error_code, result = self.run_command(check_default_db)

        check_another_db = ["check_another_db"]
        # context._models is automatically cleared after each command run and need to be set again
        context._models = custom_models
        error_code, result = self.run_command(check_another_db)

    def test_sqlite_migration_downgrade(self):
        @RootCommand.subcommand(db_untracked=True)
        def CheckDowngrade():

            engine = context._databases["default"].engine
            tables = sqlalchemy.inspect(engine).get_table_names()
            assert "task" not in tables
            assert "runtime_exception" not in tables

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "test.db"),
            }
        }

        # upgrade
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        # downgrade
        downgrade_arg_list = ["management", "db", "downgrade", "base"]
        error_code, result = self.run_command(downgrade_arg_list)

        # check if the outflow tables have been created
        check_arg_list = ["check_downgrade"]
        error_code, result = self.run_command(check_arg_list)

    def _sqlite_make_migrations(self, db_label):

        config["databases"] = {
            "default": {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, "default.db"),
            }
        }

        if db_label != "default":
            config["databases"][db_label] = {
                "dialect": "sqlite",
                "path": os.path.join(self.temp_dir_path, db_label + ".db"),
            }

        # applying initial migration
        upgrade_arg_list = ["management", "db", "upgrade", "heads"]
        error_code, result = self.run_command(upgrade_arg_list)

        # create a test plugin
        plugin_dir_path = pathlib.Path(self.temp_dir_path) / "my_plug"
        plugin_dir_path = plugin_dir_path.absolute()
        arg_list = [
            "management",
            "create",
            "plugin",
            "my_namespace.my_plugin",
            "--plugin_dir",
            plugin_dir_path.as_posix(),
        ]
        error_code, result = self.run_command(arg_list)

        # add a model to this plugin
        bind_key_str = (
            '__bind_key__ = "{db_label}"'.format(db_label=db_label)
            if db_label != "default"
            else ""
        )
        model_content = "\n".join(
            [
                "from outflow.core.db import Model",
                "from outflow.core.db.non_null_column import Column",
                "from sqlalchemy import INTEGER",
                "",
                "",
                "class Plug(Model):",
                f"    {bind_key_str}",
                "    id_plug = Column(INTEGER(), primary_key=True, nullable=False)",
                "    __tablename__ = 'plug'",
                f"    __bind_key__ = '{db_label}'" "",
            ]
        )

        with open(
            plugin_dir_path / "my_namespace" / "my_plugin" / "models" / "model.py", "w"
        ) as f:
            f.write(model_content)

        # update the pipeline settings and add the plugin to the python path
        settings.PLUGINS = ["outflow.management", "my_namespace.my_plugin"]
        sys.path.append(str(plugin_dir_path))

        # generate the migration and check the file content
        error_code, result = self.run_command(
            [
                "management",
                "db",
                "make_migrations",
                "-p",
                "my_namespace.my_plugin",
                "-d",
                db_label,
            ]
        )

        expected_migration_content = [
            "def upgrade():\n",
            "    # ### commands auto generated by Alembic - please adjust! ###\n",
            "    op.create_table(\n",
            '        "plug",\n',
            '        sa.Column("id_plug", sa.INTEGER(), nullable=False),\n',
            '        sa.PrimaryKeyConstraint("id_plug"),\n',
            "    )\n",
            '    op.grant_permissions("plug")\n',
            "    # ### end Alembic commands ###\n",
            "\n",
            "\n",
            "def downgrade():\n",
            "    # ### commands auto generated by Alembic - please adjust! ###\n",
            '    op.drop_table("plug")\n',
            "    # ### end Alembic commands ###\n",
        ]
        versions_file_list = list(
            (
                plugin_dir_path
                / "my_namespace"
                / "my_plugin"
                / "models"
                / "versions"
                / db_label
            ).glob("*.py")
        )
        with open(versions_file_list[0]) as version_file:
            migration_content = version_file.readlines()

        # check if the migration contains the expected upgrade and downgrade functions
        assert (
            migration_content[-len(expected_migration_content) :]
            == expected_migration_content
        )

    def test_sqlite_make_migrations__default_db(self):
        self._sqlite_make_migrations("default")

    def test_sqlite_make_migrations__custom_db(self):
        self._sqlite_make_migrations("custom")
