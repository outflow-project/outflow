# -*- coding: utf-8 -*-
import pytest

from outflow.core.commands import Command
from outflow.core.exceptions import IOCheckerError
from outflow.core.tasks.task import as_task
from outflow.core.test import CommandTestCase
from outflow.core.workflow import Workflow
from outflow.core.workflow.workflow import as_workflow
from outflow.library.workflows import MapWorkflow
from outflow.library.workflows.sequential_map_workflow import SequentialMapWorkflow


@as_task
def Task1() -> {"something": str}:
    return "value"


@as_task
def Task2(something: str) -> {"result": str}:
    return something + " from Task2"


@as_task
def Task2WithWrongArgs(something_else: str):
    pass


expected_result = [{"result": "value from Task2"}]


class TestWorkflow(CommandTestCase):
    def test_workflow(self):
        class MyCommand(Command):
            def setup_tasks(self):
                with Workflow() as workflow:
                    Task1() >> Task2()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()
        assert result == expected_result

    def test_workflow__iochecker_fail(self):
        class MyCommand(Command):
            def setup_tasks(self):
                with Workflow() as workflow:
                    Task1() >> Task2WithWrongArgs()

                return workflow

        self.root_command_class = MyCommand

        with pytest.raises(IOCheckerError):
            self.run_command()

    def test_workflow__as_workflow_function(self):
        class MyCommand(Command):
            def setup_tasks(self):
                @as_workflow
                def my_workflow():
                    Task1() >> Task2()

                workflow = my_workflow()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()
        assert result == expected_result

    def test_workflow__as_workflow_function__explicit_call(self):
        class MyCommand(Command):
            def setup_tasks(self):
                @as_workflow()
                def my_workflow():
                    Task1() >> Task2()

                workflow = my_workflow()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()
        assert result == expected_result

    def test_workflow__as_workflow_method(self):
        class MyCommand(Command):
            def setup_tasks(self):
                @Workflow.as_workflow
                def my_workflow():
                    Task1() >> Task2()

                workflow = my_workflow()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()
        assert result == expected_result

    def test_workflow__as_workflow_method__explicit_call(self):
        class MyCommand(Command):
            def setup_tasks(self):
                @Workflow.as_workflow()
                def my_workflow():
                    Task1() >> Task2()

                workflow = my_workflow()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()
        assert result == expected_result

    def test_workflow__MapWorkflow_generic_manual(self):
        class MyCommand(Command):
            def setup_tasks(self):

                workflow = MapWorkflow(l=["a", "b"])  # , map_on={"l": "something"})
                workflow.start()
                Task2()
                workflow.map_on = {"l": "something"}
                workflow.stop()

                return workflow

        self.root_command_class = MyCommand

        _, result = self.run_command()

        assert result == [{"result": ["a from Task2", "b from Task2"]}]

    def test_workflow__MapWorkflow_generic_as_decorator(self):
        @MapWorkflow.as_workflow(l=["a", "b"], map_on={"l": "something"})
        def workflow():
            Task2()

        class MyCommand(Command):
            def setup_tasks(self):
                return workflow()

        self.root_command_class = MyCommand

        _, result = self.run_command()

        assert result == [{"result": ["a from Task2", "b from Task2"]}]

    def test_workflow_external_edge(self):
        @as_task
        def Task1() -> {"something_else": str}:
            return " something else"

        @as_task
        def Task2(something: str, something_else=" nothing") -> {"result": str}:
            return something + " from Task2" + something_else

        @SequentialMapWorkflow.as_workflow(l=["a", "b"], map_on={"l": "something"})
        def workflow(t1):
            t1 | Task2()

        class MyCommand(Command):
            def setup_tasks(self):
                t1 = Task1()
                return workflow(t1)

        self.root_command_class = MyCommand

        _, result = self.run_command()

        assert result == [
            {"result": ["a from Task2 something else", "b from Task2 something else"]}
        ]
