# -*- coding: utf-8 -*-
from typing import List

import pytest

from outflow.core.commands import Command
from outflow.core.exceptions import (
    ContextArgumentException,
    TaskWithKwargsException,
)
from outflow.core.pipeline import config
from outflow.core.target import Target
from outflow.core.tasks import Task, as_task
from outflow.core.test import TaskTestCase, CommandTestCase
from outflow.core.types import outputs_type_factory, Parameter


@as_task
def ReturnNoneAsTask():
    pass


class ReturnNone(Task):
    def run(self):
        pass


@Target.output("result")
@as_task
def ComputeSumAsTask(
    small_array: List[int],
) -> outputs_type_factory({"result": int}):
    return {"result": sum(small_array)}


@Target.output("result")
@as_task(with_self=True)
def ComputeSumAsTaskWithContext(self, small_array: List[int]):
    return {"result": sum(small_array)}


class ComputeSum(Task):
    def run(self, small_array: List[int]):
        return {"result": sum(small_array)}


@as_task
def TaskWithParamAsTask(
    sep: str, param_a: Parameter(str), param_b: Parameter(str)
) -> {"output_ab": str}:
    result = param_a + sep + param_b
    return {"output_ab": result}


class TaskWithParam(Task):
    def run(self, sep: str, param_a: Parameter(str), param_b: Parameter(str)):
        result = param_a + sep + param_b
        return {"output_ab": result}


@Target.parameters("param_a", "param_b")
class TaskWithLegacyParam(Task):
    def run(self, sep: str, param_a: str, param_b: str):
        result = param_a + sep + param_b
        return {"output_ab": result}


class TestOutflowTask(TaskTestCase):
    def test_task_name(self):
        assert ComputeSum.name == "compute_sum"

    def test_task_name_as_task(self):
        assert ComputeSumAsTask.name == "compute_sum_as_task"

    def test_context_exception(self):
        with pytest.raises(ContextArgumentException):

            class TaskWithSelfMissing(Task):
                # testing missing 'self' argument
                def run(arg1):
                    return {"result": []}

            TaskWithSelfMissing()

    def test_task_with_kwgs(self):
        with pytest.raises(TaskWithKwargsException):

            class TaskWithKwargs(Task):
                def run(self, *, arg1):
                    return {"result": []}

            TaskWithKwargs()

    def test_task_with_default_args(self):
        class TaskWithDefaultValue(Task):
            def run(self, arg1="aaa"):
                return {"result": arg1}

        self.task = TaskWithDefaultValue()
        result = self.run_task()

        assert result == {"result": "aaa"}

    def test_task_with_defaukt_kwgs(self):
        with pytest.raises(TaskWithKwargsException):

            class TaskWithKwargs(Task):
                def run(self, *, arg1="aaa"):
                    return {"result": []}

            TaskWithKwargs()

    def test_task_with_input_as_task(self):
        self.task = ComputeSumAsTask()
        result = self.run_task(small_array=[1, 2, 3])

        assert result == {"result": 6}

    def test_task_with_input_as_task_with_self(self):
        self.task = ComputeSumAsTaskWithContext()
        result = self.run_task(small_array=[1, 2, 3])

        assert result == {"result": 6}

    def test_task_with_input(self):

        self.task = ComputeSum()
        result = self.run_task(small_array=[1, 2, 3])

        assert result == {"result": 6}

    def test_task_with_parameters_as_task(self):
        config.update(
            {
                "parameters": {
                    "task_with_param_as_task": {
                        "param_a": "aaa",
                        "param_b": "bbb",
                    }
                }
            }
        )

        self.task = TaskWithParamAsTask()
        result = self.run_task(sep="_")

        assert result == {"output_ab": "aaa_bbb"}

    def test_task_with_parameters(self):
        config.update(
            {
                "parameters": {
                    "task_with_param": {
                        "param_a": "aaa",
                        "param_b": "bbb",
                    }
                }
            }
        )

        self.task = TaskWithParam()
        result = self.run_task(sep="_")

        assert result == {"output_ab": "aaa_bbb"}

    def test_task_with_legacy_parameters(self):
        config.update(
            {
                "parameters": {
                    "task_with_legacy_param": {
                        "param_a": "aaa",
                        "param_b": "bbb",
                    }
                }
            }
        )

        self.task = TaskWithLegacyParam()
        result = self.run_task(sep="_")

        assert result == {"output_ab": "aaa_bbb"}

    def test_task_with_type_error_as_task(self):
        self.task = ComputeSumAsTask()
        with pytest.raises(TypeError):
            self.run_task(small_array={1, 2, 3})

    def test_task_with_type_error(self):
        self.task = ComputeSum()
        with pytest.raises(TypeError):
            self.run_task(small_array={1, 2, 3})

    def test_return_none(self):
        self.task = ReturnNone()
        result = self.run_task()
        assert result is None

    def test_return_none_as_task(self):
        self.task = ReturnNoneAsTask()
        result = self.run_task()
        assert result is None


@as_task
def GenArray() -> {"small_array": list}:
    return {"small_array": [1, 2, 3]}


class LShiftCommand(Command):
    """
    Test lshift operator
    """

    def setup_tasks(self):
        ComputeSum() << GenArray()


class TestOutflowTasksInCommands(CommandTestCase):
    db_untrack = True

    def test_lshift_operator(self):
        self.root_command_class = LShiftCommand

        self.run_command()
