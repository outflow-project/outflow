# -*- coding: utf-8 -*-
if __name__ == "__main__":
    from .management.entrypoint import entrypoint

    exit(entrypoint())
