#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ## Mandatory code in every plugin.models __init__.py needed for alembic
# ## DO NOT EDIT

# import importlib
# import os
# import pkgutil
#
# pkg_dir = os.path.dirname(__file__)
# for (module_loader, name, ispkg) in pkgutil.iter_modules([pkg_dir]):
#     importlib.import_module("." + name, __package__)


# ## End of mandatory code
