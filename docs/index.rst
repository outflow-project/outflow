Welcome to the Outflow documentation!
=====================================

.. figure:: sections/images/logo.svg
   :alt: Outflow logo
   :align: center
   :width: 500px

|python| |PyPI| |pipeline status| |coverage report| |image4| |Documentation Status| |license| |chat discord support|

.. |python| image:: https://img.shields.io/pypi/pyversions/outflow.svg
   :target: https://pypi.org/project/outflow/
.. |PyPI| image:: https://img.shields.io/pypi/v/outflow
   :target: https://pypi.org/project/outflow/
.. |pipeline status| image:: https://gitlab.com/outflow-project/outflow/badges/master/pipeline.svg
   :target: https://gitlab.com/outflow-project/outflow/
.. |coverage report| image:: https://gitlab.com/outflow-project/outflow/badges/master/coverage.svg
   :target: https://gitlab.com/outflow-project/outflow/
.. |image4| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/ambv/black
.. |Documentation Status| image:: https://readthedocs.org/projects/outflow/badge/?version=latest
   :target: https://outflow.readthedocs.io/en/latest/?badge=latest
.. |license| image:: https://img.shields.io/pypi/l/outflow.svg
   :target: https://pypi.python.org/pypi/outflow
.. |chat discord support| image:: https://img.shields.io/badge/discord-support-7389D8?logo=discord&style=flat&logoColor=fff
   :target: https://discord.outflow.dev


Outflow is a framework that helps you build and run task workflows.

The API is as simple as possible while still giving the user full control over the definition and execution of the workflows.

**Feature highlight :**

-  Simple but powerful API
-  Support for **parallelized and distributed execution**
-  Centralized **command line interface** for your pipeline commands
-  Integrated **database** access, sqlalchemy models and alembic migrations
-  Executions and exceptions logging for **tracability**
-  Strict type and input/output checking for a **robust** pipeline

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview<sections/01_overview>
   Tutorial<sections/02_tutorial/index>
   User guide<sections/03_user_guide/index>
   Quick reference<sections/05_quick_reference>
   API<sections/06_api>



..
    Indices and tables
    ===================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
