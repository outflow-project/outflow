# Commands

## Create a command

To create a first level command, import `from outflow.core.commands import Command, RootCommand` and subclass Command and decorate with `@RootCommand.subcommand()`:

```python
from outflow.core.commands import Command, RootCommand

@RootCommand.subcommand()
class MyCommand(Command):

    def setup_tasks(self):
        # instantiate tasks and setup the workflow
```

This command will be available at :

```
$ python manage.py my_command
```

## Create subcommands

If you want to create subcommands of a common command (think `git clone` `git commit`), first create a non-invokable top level command:

```python
@RootCommand.subcommand(invokable=False)
class Git(Command):
    pass
```

This command will print its help if called directly.

Then, create a subcommand of the previous one.

```python
@Git.subcommand(invokable=False)
class Clone(Command):
    def setup_tasks(self):
        ...
```

## Built-in commands

Outflow ships with a bunch of useful commands, available through the `management` command :

```bash
$ python -m outflow management ...
or
$ python manage.py management ...
```

(ShellCommand)=

### shell

`python manage.py management shell`

This command will execute an `IPythonTask`. This is useful for development and debugging, because you are inside an outflow pipeline so you have acces to everything you would in a pipeline execution : the pipeline context, the database session, the config and settings. You can also import tasks and execute them like so (if you provide them with the expected inputs)

```shell
In [1]: from namespace.plugin.tasks import FirstTask, SecondTask
In [2]: first_task = FirstTask() # instanciate the task
In [3]: my_input = 42
In [4]: first_task_result = first_task(input1=my_input) # call the task with the inputs as kwargs
In [5]: second_task = SecondTask()
In [6]: second_task(first_task_result) # tasks return dictionaries so you can call the next task of the workflow directly with the result of the previous task
```

### display_config

`python manage.py management display_config`

Prints the path of the configuration file.

[//]: # "### db"
