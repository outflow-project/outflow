Outflow standard task and workflow library
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Outflow standard task and workflow library

   Tasks<tasks>
   Workflows<workflows>
