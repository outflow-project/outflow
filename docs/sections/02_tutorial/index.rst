Tutorial
=====================================

.. toctree::
    :maxdepth: 2
    :caption: Tutorial:

    Part 1: Pipeline and plugins<part1_pipeline_and_plugins>
    Part 2: Tasks and commands<part2_tasks_and_commands>
    Part 3: Workflow and caching <part3_workflow_and_caching>
    Part 4: Parallelize your workflows <part4_parallelize_your_workflows>
    Part 5: Models and database <part5_models_and_database>
    Part 6: Products <part6_products>
    Part 7: Testing<part7_testing>
